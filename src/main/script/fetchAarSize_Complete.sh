#!/bin/bash

start=$(date +%s)
#For formatting:
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
#Usage : echo "This text is ${RED}red${NONE} and ${GREEN}green${NONE} and ${BOLD}bold${NONE} and ${UNDERLINE}underlined${NONE}."
PATH=/bin:/usr/bin:
NONE='\033[00m'
#FOREGROUND TEXT
BLACK='\033[01;30m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
BLUE='\033[01;34m'
MEGENTA='\033[01;35m'
CYAN='\033[01;36m'
L_GRAY='\033[01;37m'
D_GRAY='\033[01;90m'
L_RED='\033[01;91m'
L_GREEN='\033[01;92m'
L_YELLOW='\033[01;93m'
L_BLUE='\033[01;94m'
L_MEGENTA='\033[01;95m'
L_CYAN='\033[01;96m'
WHITE='\033[01;97m'
#BACKGROUND TEXT
B_BLACK='\033[01;40m'
B_RED='\033[01;41m'
B_GREEN='\033[01;42m'
B_YELLOW='\033[01;43m'
B_BLUE='\033[01;44m'
B_MAGENTA='\033[01;45m'
B_CYAN='\033[01;46m'
B_GRAY='\033[01;100m'
B_WHITE='\033[01;101m'
B_L_GRAY='\033[01;47m'
B_L_RED='\033[01;101m'
B_L_GREEN='\033[01;102m'
B_L_YELLOW='\033[01;103m'
B_L_BLUE='\033[01;104m'
B_L_MAGENTA='\033[01;105m'
B_L_CYAN='\033[01;106m'
#OTERHS
BOLD='\033[1m'
DIM='\033[2m'
ITALICS='\033[3m'
UNDERLINE='\033[4m'
BLINK='\033[5m'
INVERTED='\033[7m'
HIDDEN='\033[8m'

ARG_COUNT=$#
VAR1=$1
VAR2=$2

run_check() {
  if [ ${ARG_COUNT} -eq 0 ]; then
    printf "${RED}"
    echo "+——————————————————————————————————————————————————————+"
    printf "│      %-47s │\n" "$(date)"
    echo "+——————————————————————————————————————————————————————+"
    echo "│ No arguments supplied                                │"
    printf "$(tput setaf 1)│$(tput bold) %-52s │$(tput sgr0)\n" "USAGE:"
    printf "$(tput setaf 1)│$(tput bold) %-52s │$(tput sgr0)\n" "sh $0 <Path to Android Repository>"
    printf "${RED}"
    echo "+——————————————————————————————————————————————————————+"
    printf "$NONE"
    echo
    exit 1
  else
    ANDROID_DIR_PATH=$VAR1
    folder_name=$(basename "${ANDROID_DIR_PATH}")
  fi
}

step_info() {
  printf "\n"
  printf "${L_YELLOW}"
  echo "+—————————————————————————————————————+"
  printf "│     %-31s │\n" "$(date)"
  echo "+—————————————————————————————————————+"
  echo "│                                     │"
  printf "│$(tput bold) %-35s │$(tput sgr0)\n" "$@"
  printf "${L_YELLOW}"
  echo "+—————————————————————————————————————+"
  printf "${NONE}"
}

step_info_seperator() {
  cols=$(tput cols)
  printf "\n"
  for ((j = 0; j < cols; j++)); do printf "\e[38;5;${j}m#\e[0m"; done
  echo
}

#Script
run_check
# ANDROID_DIR_PATH="/Users/tusharrastogi/AndroidApp/paytm-android-app"
printf "Android folder directory : ${folder_name}\n"
cd ${ANDROID_DIR_PATH}
branch_Name=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
printf "Current Branch : ${branch_Name}\n"

if [ ${ARG_COUNT} -eq 2 ]; then
  printf "${BOLD}Branch name provided as${NONE} :  ${BLINK}${VAR2}${NONE}\n"
  if [[ ${branch_Name} == ${VAR2} ]]; then
    printf "Specifed branch already checkedout, working on branch : ${branch_Name}\n"
    printf "${BLUE}--Fetching from remote--${NONE}\n"
    if ! git fetch --all; then
      echo >&2 ${RED}Check your VPN connection${NONE}
      exit 1
    fi
    printf "${BLUE}--Pulling branch ${BLINK}${branch_Name}${NONE}${BLUE} from remote${NONE}--\n"
    git pull origin ${branch_Name}
  else
    printf "Currently on branch \"${branch_Name}\", checking out to specifed branch ${BLINK}${VAR2}${NONE}\n"
    printf "${BLUE}--Fetching from remote--${NONE}\n"
    if ! git fetch --all; then
      echo >&2 ${RED}Check your VPN connection${NONE}
      exit 1
    fi
    printf "${BLUE}--Checkout branch ${BLINK}${VAR2}${NONE}${BLUE} from remote--${NONE}\n"
    git checkout ${VAR2}
    branch_Name=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    if [[ ${branch_Name} == ${VAR2} ]]; then
      printf "Switched to branch : ${BOLD}${branch_Name}${NONE}\n"
      printf "${BLUE}--Pulling branch ${BLINK}${branch_Name}${NONE}${BLUE} from remote--${NONE}\n"
      git pull origin ${branch_Name}
    else
      printf "${RED}Some issue occured${NONE}\n"
      exit 1
    fi
  fi
else
  read -p "Do you want to continue with the current branch name, type 'y' if yes else enter branch name : " select_current_branch
  if [[ "${select_current_branch}" =~ y ]]; then
    printf "Working on already selected branch : ${branch_Name}\n"
    printf "${BLUE}--Fetching from remote--${NONE}\n"
    if ! git fetch --all; then
      echo >&2 ${RED}Check your VPN connection${NONE}
      exit 1
    fi
    printf "${BLUE}--Pulling branch ${BLINK}${branch_Name}${NONE} ${BLUE}from remote--${NONE}\n"
    git pull origin ${branch_Name}
  else
    printf "${BLUE}--Fetching from remote--${NONE}\n"
    if ! git fetch --all; then
      echo >&2 ${RED}Check your VPN connection${NONE}
      exit 1
    fi
    printf "${BLUE}--Checkout branch ${BLINK}${select_current_branch}${NONE}${BLUE} from remote--${NONE}\n"
    git checkout ${select_current_branch}
    branch_Name=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    printf "Switched to branch : ${BOLD}${branch_Name}${NONE}\n"
    if [[ "${branch_Name}" == "${select_current_branch}" ]]; then
      printf "Branch name same\n"
    else
      printf "Branch name not same\n"
      exit
    fi
    printf "${BLUE}--Pulling branch ${BLINK}${select_current_branch}${NONE}${BLUE} from remote--${NONE}\n"
    git pull origin ${select_current_branch}
  fi
fi

#Build clean
step_info "Cleaning project"
./gradlew clean

#Creating appmanager release artifact
step_info "Generating aar for \"appmanager\""
./gradlew appmanager:bundleReleaseAar
cd ${ANDROID_DIR_PATH}/appmanager/build/outputs/aar
appmanager_aar_size=$(du -sh appmanager-release.aar | cut -f1)
cd -

#Creating home release artifact
# <string name="schema_name">paytmmp</string>
step_info "Generating aar for \"home\""
./gradlew home:bundleReleaseAar
cd ${ANDROID_DIR_PATH}/home/build/outputs/aar
home_aar_size=$(du -sh home-release.aar | cut -f1)
cd -

#Creating paytmGamepind release artifact
step_info "Generating aar for \"Gamepind\""
./gradlew :features:paytmGamepind:bundleReleaseAar
cd ${ANDROID_DIR_PATH}/features/paytmGamepind/build/outputs/aar
gamePind_aar_size=$(du -sh paytmGamepind-release.aar | cut -f1)
cd -

step_info_seperator
printf "${BOLD}Stats for branch :${NONE} ${B_L_BLUE}${L_YELLOW}${branch_Name}${NONE}\n"
printf "\"appmanager\" release aar file size : ${INVERTED}${WHITE}${appmanager_aar_size}${NONE}\n"
printf "\"home\" release aar file size : ${INVERTED}${WHITE}${home_aar_size}${NONE}\n"
printf "\"Gamepind\" release aar file size : ${INVERTED}${WHITE}${gamePind_aar_size}${NONE}"
step_info_seperator
printf "\n"
end=$(date +%s)
runtime=$((${end} - ${start}))
function show_time() {
  local T=$1
  local D=$((T / 60 / 60 / 24))
  local H=$((T / 60 / 60 % 24))
  local M=$((T / 60 % 60))
  local S=$((T % 60))
  (($D > 0)) && printf '%d days ' $D
  (($H > 0)) && printf '%d hours ' $H
  (($M > 0)) && printf '%d minutes ' $M
  (($D > 0 || $H > 0 || $M > 0)) && printf 'and '
  printf '%d seconds\n' $S
}
printf "Total time for execution : "
show_time ${runtime}
printf "\n"

exit 0

GradleFile=/Users/tusharrastogi/StudioProjects/paytm-android-app/build.gradle
artifactMapperFile=/Users/tusharrastogi/Desktop/Utility/artifactMapper.properties
# awk NF /Users/tusharrastogi/StudioProjects/paytm-android-app/build.gradle | awk '/ ext {/{flag=1; next} /}/{flag=0} flag' | awk '{$1=$1};1'
awk NF ${GradleFile} | awk '/ ext {/{flag=1; next} /}/{flag=0} flag' | awk '{$1=$1};1' >${artifactMapperFile}
