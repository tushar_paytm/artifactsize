#!/bin/bash

echo "Starting script"

ARG_COUNT=$#
VAR1=$1
VAR2=$2
VAR3=$3
SCRIPT_DIR=$(pwd)

echo "$1"
echo "$2"

run_check() {
  if [ ${ARG_COUNT} -eq 0 ]; then
    echo "No arguments supplied"
    exit 1
  else
    ANDROID_DIR_PATH=$VAR1
    folder_name=$(basename "${ANDROID_DIR_PATH}")
  fi
}

#Script
run_check
echo "$ANDROID_DIR_PATH"
echo "{$folder_name}"
cd "${ANDROID_DIR_PATH}"
case $VAR3 in
clean)
  branch_Name=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
  echo "$branch_Name"
  if [[ ${branch_Name} == ${VAR2} ]]; then
    echo "In if"
    echo "Fetch"
    if ! git fetch --all; then
      echo >&2 Check your VPN connectionremote
      exit 1
    fi
    echo "Pull"
    git pull origin ${branch_Name}
  else
    echo "In else"
    echo "Fetch"
    if ! git fetch --all; then
      echo >&2 Check your VPN connectionremote
      exit 1
    fi
    echo "Checkout"
    git checkout ${VAR2}
    branch_Name=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    if [[ ${branch_Name} == ${VAR2} ]]; then
      echo "Pull"
      git pull origin ${branch_Name}
    else
      exit 1
    fi
  fi
  #Build clean
  echo "Cleaning build"
  ./gradlew clean
  echo "Cleaning build completed"
  ;;
home)
  echo "Home"
  #Creating home release artifact
  # <string name="schema_name">paytmmp</string>
  ./gradlew home:bundleReleaseAar
  cd "${ANDROID_DIR_PATH}"/home/build/outputs/aar
  home_aar_size=$(du -sh home-release.aar | cut -f1)
  echo "home aarSize : ${home_aar_size}"
  cd -
  ;;
appmanager)
  echo "appmanager"
  # Creating appmanager release artifact
  ./gradlew appmanager:bundleReleaseAar
  echo "cd ${ANDROID_DIR_PATH}/appmanager/build/outputs/aar"
  cd "${ANDROID_DIR_PATH}"/appmanager/build/outputs/aar
  appmanager_aar_size=$(du -sh appmanager-release.aar | cut -f1)
  echo "appmanager aarSize : ${appmanager_aar_size}"
  cd -
  ;;
paytmGamepind)
  echo "paytmGamepind"
  # Creating paytmGamepind release artifact
  ./gradlew :features:paytmGamepind:bundleReleaseAar
  cd "${ANDROID_DIR_PATH}"/features/paytmGamepind/build/outputs/aar
  gamePind_aar_size=$(du -sh paytmGamepind-release.aar | cut -f1)
  echo "paytmGamepind aarSize : ${gamePind_aar_size}"
  cd -
  ;;
fetchBuildGradle)
  echo "fetchBuildGradle"
  GradleFile=${ANDROID_DIR_PATH}/build.gradle
  artifactMapperFile=${SCRIPT_DIR}/artifactMapper.properties
  awk NF "${GradleFile}" | awk '/ ext {/{flag=1; next} /}/{flag=0} flag' | awk '{$1=$1};1' > "${artifactMapperFile}"
  ;;
esac
