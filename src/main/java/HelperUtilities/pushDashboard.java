package HelperUtilities;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;


public class pushDashboard {


    public static void RegistrationSuccessful(String appStamp,Object verticalName, Object artifactName, Object artifactVersion, float finalSize)
    {
        RestAssured.baseURI = "https://appqa-dashboard.mypaytm.com:8080/paytm/automation/v1/performance/publishartifactsize";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("appStamp", appStamp);
        requestParams.put("verticalName", verticalName);
        requestParams.put("artifactName", artifactName);
        requestParams.put("artifactVersion", artifactVersion);
        requestParams.put("size", finalSize);
        requestParams.put("platformName",  "Android");
        request.body(requestParams.toJSONString());
        Response response = request.post();
        System.out.println("Data Pushed to Metabase: " +  response);
    }


}
