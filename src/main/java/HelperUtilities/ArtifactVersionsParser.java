package HelperUtilities;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArtifactVersionsParser {

    InputStream inputStream;
    HashMap<Object, String> propMap;

    public HashMap<Object, String> getPropValues(String propFileName) {
        Properties prop = null;
        propMap = new HashMap<>();
        try {
            prop = new Properties();
//            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            inputStream = new FileInputStream(new File("./" + propFileName));
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (Object key : prop.keySet()) {
            String val = prop.getProperty(key.toString());
            if (val.contains("\"") || val.contains("'")) {
                val = getValue(val);
            }
            propMap.put(key, val);
        }
        return propMap;
    }

    public String getValue(String val) {
        final String regex = "[\\\"'](.+)[\\\"']";
        final String string = val;
        String val1 = null;
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            val1 = matcher.group(1);
        }
        return val1;
    }


}
