package HelperUtilities;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.*;

public class GoogleSheetsHelper {

    String spreadsheetId;
    Sheets service;

    private static final String APPLICATION_NAME = "Google Sheets API";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "GoogleSheetAPIToken";

    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    private static NetHttpTransport HTTP_TRANSPORT;

    private static int sheetID;

    public GoogleSheetsHelper(String spreadsheetId) {
        this.spreadsheetId = spreadsheetId;
        try {
            service = getSheetsService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Sheets getSheetsService() throws IOException {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Credential credential = getCredentials(HTTP_TRANSPORT);
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).setHttpRequestInitializer(setHttpTimeout(credential))
                .build();
    }

    //To handle SocketTimeoutException
    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setConnectTimeout(3 * 60000);  // 3 minutes connect timeout
                httpRequest.setReadTimeout(3 * 60000);  // 3 minutes read timeout
            }
        };
    }

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = GoogleSheetsHelper.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public void createSheet(String title) {
        Spreadsheet spreadsheet = new Spreadsheet().setProperties(new SpreadsheetProperties().setTitle(title));
        try {
            spreadsheet = service.spreadsheets().create(spreadsheet).setFields("spreadsheetId").execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Spreadsheet ID: " + spreadsheet.getSpreadsheetId());

    }

    public static void generateSheetID() {
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        sheetID = number;
        System.out.println("Sheet URL : \n" + "https://docs.google.com/spreadsheets/d/1e8dSJyeWuqhXsMmpVODuF5SL1cX3xB1S5Jk5STj0-Ro/edit#gid=" + sheetID + "\n");
    }

    public enum ValueInputOption {RAW, USER_ENTERED}

    public BatchUpdateValuesResponse sheetBatchWrite(String SheetName, String start, String end, List<List<Object>> data, ValueInputOption valueInputOption) {
        String range;
        if (end == "") {
            range = SheetName + "!" + start;
        } else {
            range = SheetName + "!" + start + ":" + end;
        }
        List<List<Object>> arrData = data;
        ValueRange oRange = new ValueRange();
        oRange.setRange(range);
//        oRange.setMajorDimension("COLUMNS");
        oRange.setValues(arrData);

        List<ValueRange> oList = new ArrayList<>();
        oList.add(oRange);

        BatchUpdateValuesRequest oRequest = new BatchUpdateValuesRequest();

        if (valueInputOption.equals(ValueInputOption.RAW)) {
            oRequest.setValueInputOption("RAW");
        } else {
            oRequest.setValueInputOption("USER_ENTERED");
        }
        oRequest.setData(oList);
        BatchUpdateValuesResponse batchUpdateValuesResponse = null;
        try {
            batchUpdateValuesResponse = service.spreadsheets().values().batchUpdate(spreadsheetId, oRequest).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return batchUpdateValuesResponse;
    }


    public void sheetWrite(String SheetName, String start, String end, List<List<Object>> data, ValueInputOption valueInputOption) {
        String range = SheetName + "!" + start + ":" + end;
        System.out.println(range);
        List<List<Object>> arrData = data;
        ValueRange oRange = new ValueRange();
        oRange.setRange(range);
//        oRange.setMajorDimension("COLUMNS");
        oRange.setValues(arrData);

        List<ValueRange> oList = new ArrayList<>();
        oList.add(oRange);

        UpdateValuesResponse oRequest = new UpdateValuesResponse();

        try {
            Sheets.Spreadsheets.Values.Update request = service.spreadsheets().values().update(spreadsheetId, range, oRange);
            request.setValueInputOption(String.valueOf(valueInputOption)).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String addSheet(String sheetName, int rowCount, int coloumnCount, int frozenRowCount, int frozenColumnCount) {

        generateSheetID();

        //Check if same name sheet already exists
        Spreadsheet spreadsheet = null;
        try {
            spreadsheet = service.spreadsheets().get(spreadsheetId).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Sheet> sheetNameList = spreadsheet.getSheets();
        //System.out.println(sheetNameList);
        Iterator<Sheet> iterator = sheetNameList.iterator();

        while (iterator.hasNext()) {
            Sheet sheet = iterator.next();
            if (sheet.getProperties().getTitle().equalsIgnoreCase(sheetName)) {
                sheetName = sheetName + "_" + sheetID;
                Properties prop = new Properties();
                FileInputStream configStream = null;
                try {
                    configStream = new FileInputStream(System.getProperty("user.dir") + "/Input.properties");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    prop.load(configStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                prop.setProperty("sheetName", sheetName);
                FileOutputStream output = null;
                try {
                    output = new FileOutputStream(System.getProperty("user.dir") + "/Input.properties");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    prop.store(output, "Sheet updated as sheetName already exists");
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        GridProperties gp = new GridProperties();
        if (coloumnCount != 0) gp.setColumnCount(coloumnCount);
        gp.setRowCount(rowCount);
        gp.setFrozenRowCount(frozenRowCount);
        gp.setFrozenColumnCount(frozenColumnCount);

        SheetProperties sp = new SheetProperties();
        sp.setTitle(sheetName);
        sp.setGridProperties(gp);
        sp.setSheetId(sheetID);

        AddSheetRequest asq = new AddSheetRequest();
        asq.setProperties(sp);

        Request req3 = new Request();
        req3.setAddSheet(asq);

        List<Request> requests = new ArrayList<>();
        requests.add(req3);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sheetName;


    }

    /**
     * Prints the names and majors of students in a sample spreadsheet:
     * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
     *
     * @return
     */
    public List<List<Object>> sheetRead(String spreadsheetId, String range){
        // Build a new authorized API client service.

//        final String spreadsheetId = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms";
//        final String range = "Class Data!A2:E";
        ValueRange response = null;
        try {
            response = service.spreadsheets().values()
                    .get(spreadsheetId, range)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<List<Object>> values = response.getValues();
        /*if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            System.out.println("Name, Major");
            for (List row : values) {
                // Print columns A and E, which correspond to indices 0 and 4.
                System.out.printf("%s, %s\n", row.get(0), row.get(4));
            }
        }*/
        return values;
    }

    public MergeCellsRequest mergeColoumns(int startColoumn, int endColoumn, int startRow, int endRow) {

        GridRange mgr = new GridRange();
        mgr.setStartColumnIndex(startColoumn);
        mgr.setStartRowIndex(startRow);
        mgr.setEndRowIndex(endRow);
        mgr.setEndColumnIndex(endColoumn);
        mgr.setSheetId(sheetID);


        MergeCellsRequest msr = new MergeCellsRequest();
        msr.setRange(mgr);
        msr.setMergeType("MERGE_ROWS");

        Request req = new Request();
        req.setMergeCells(msr);

//        Request req1 = new Request();
//        req1.setAddNamedRange(new AddNamedRangeRequest().setNamedRange(new NamedRange().setName("Device_NAME").setRange(gr)));

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msr;
    }

    public MergeCellsRequest mergeRows(int startRow, int endRow, int startColoumn, int endColoumn) {

        GridRange gr = new GridRange();
        gr.setStartColumnIndex(startColoumn);
        gr.setStartRowIndex(startRow);
        gr.setEndRowIndex(endRow);
        gr.setEndColumnIndex(endColoumn);
        gr.setSheetId(sheetID);

        MergeCellsRequest msr = new MergeCellsRequest();
        msr.setRange(gr);
        msr.setMergeType("MERGE_COLUMNS");

        Request req = new Request();
        req.setMergeCells(msr);

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msr;
    }

    public BatchUpdateSpreadsheetResponse mergeAll(int startRow, int endRow, int startColoumn, int endColoumn) {

        GridRange gr = new GridRange();
        gr.setStartColumnIndex(startColoumn);
        gr.setStartRowIndex(startRow);
        gr.setEndRowIndex(endRow);
        gr.setEndColumnIndex(endColoumn);
        gr.setSheetId(sheetID);

        MergeCellsRequest msr = new MergeCellsRequest();
        msr.setRange(gr);
        msr.setMergeType("MERGE_ALL");

        Request req = new Request();
        req.setMergeCells(msr);

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);
        BatchUpdateSpreadsheetResponse batchUpdateSpreadsheetResponse = null;
        try {
            batchUpdateSpreadsheetResponse = service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return batchUpdateSpreadsheetResponse;
    }

    public void setHeader(int startRow, int endRow, int startColoumn, int endColoumn) {

        GridRange gr = new GridRange();
        gr.setStartRowIndex(startRow);
        gr.setEndRowIndex(endRow);
        gr.setStartColumnIndex(startColoumn);
        gr.setEndColumnIndex(endColoumn);
        gr.setSheetId(sheetID);

        //Refer this site to get color values in Float : https://corecoding.com/utilities/rgb-or-hex-to-float.php
        Color backgroundColor = new Color();
        backgroundColor.setRed(0F);
        backgroundColor.setGreen(0.306F);
        backgroundColor.setBlue(0.6F);
        backgroundColor.setAlpha(1F);

        Color foregroundColor = new Color();
        foregroundColor.setRed(1F);
        foregroundColor.setGreen(0.898F);
        foregroundColor.setBlue(0.788F);
        foregroundColor.setAlpha(1F);

        TextFormat textFormat = new TextFormat();
        textFormat.setBold(true);
        textFormat.setFontSize(11);
        textFormat.setFontFamily("Impact");
        textFormat.setForegroundColor(foregroundColor);

        CellFormat cellFormat = new CellFormat();
        cellFormat.setVerticalAlignment("middle");
        cellFormat.setHorizontalAlignment("center");
        cellFormat.setBackgroundColor(backgroundColor);
        cellFormat.setTextFormat(textFormat);

        CellData cellData = new CellData();
        cellData.setUserEnteredFormat(cellFormat);

        RepeatCellRequest repeatCellRequest = new RepeatCellRequest();
        repeatCellRequest.setRange(gr);
        repeatCellRequest.setCell(cellData);
        repeatCellRequest.setFields("userEnteredFormat(backgroundColor,textFormat,horizontalAlignment,verticalAlignment)");

        Request req = new Request();
        req.setRepeatCell(repeatCellRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateCellFormat(GridRange gr, CellFormat cellFormat) {

        CellData cellData = new CellData();
        cellData.setUserEnteredFormat(cellFormat);

        RepeatCellRequest repeatCellRequest = new RepeatCellRequest();
        repeatCellRequest.setRange(gr);
        repeatCellRequest.setCell(cellData);
        repeatCellRequest.setFields("userEnteredFormat(backgroundColor,textFormat,horizontalAlignment,verticalAlignment,TextRotation)");

        Request req = new Request();
        req.setRepeatCell(repeatCellRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deviceNameCellFormat(GridRange gridRange) {
        Color foregroundColor = new Color();
        foregroundColor.setRed(0F);
        foregroundColor.setGreen(0.306F);
        foregroundColor.setBlue(0.6F);
        foregroundColor.setAlpha(1F);

        Color backgroundColor = new Color();
        backgroundColor.setRed(1F);
        backgroundColor.setGreen(0.898F);
        backgroundColor.setBlue(0.788F);
        backgroundColor.setAlpha(1F);

        TextFormat textFormat = new TextFormat();
        textFormat.setBold(true);
        textFormat.setFontSize(11);
        textFormat.setFontFamily("Trebuchet MS");
        textFormat.setForegroundColor(foregroundColor);

        CellFormat cellFormat = new CellFormat();
        cellFormat.setBackgroundColor(backgroundColor);
        cellFormat.setHorizontalAlignment("center").setVerticalAlignment("middle");
        cellFormat.setTextDirection("RIGHT_TO_LEFT");
//        cellFormat.setTextRotation(new TextRotation().setAngle(45));
        cellFormat.setTextFormat(textFormat);

        updateCellFormat(gridRange, cellFormat);
    }

    public void timeCellFormat() {

        GridRange gridRange = new GridRange();
        gridRange.setStartColumnIndex(0);
        gridRange.setEndColumnIndex(3);
        gridRange.setStartRowIndex(1);
        gridRange.setSheetId(sheetID);

        TextFormat textFormat = new TextFormat();
        textFormat.setFontSize(11);
        textFormat.setFontFamily("Calibri");

        CellFormat cellFormat = new CellFormat();
        cellFormat.setHorizontalAlignment("center").setVerticalAlignment("middle");
        cellFormat.setTextFormat(textFormat);

        updateCellFormat(gridRange, cellFormat);
    }


    public void resizeColoumnAutofit() {
        DimensionRange dimensionRange = new DimensionRange();
        dimensionRange.setStartIndex(0).setSheetId(sheetID).setDimension("COLUMNS");

        AutoResizeDimensionsRequest autoResizeDimensionsRequest = new AutoResizeDimensionsRequest();
        autoResizeDimensionsRequest.setDimensions(dimensionRange);

        Request request = new Request();
        request.setAutoResizeDimensions(autoResizeDimensionsRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(request);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addColumnatEnd(int length) {

        AppendDimensionRequest appendDimensionRequest = new AppendDimensionRequest();
        appendDimensionRequest.setSheetId(sheetID);
        appendDimensionRequest.setDimension("COLUMNS");
        appendDimensionRequest.setLength(length);

        Request request = new Request();
        request.setAppendDimension(appendDimensionRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(request);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addRowatEnd(int length) {

        AppendDimensionRequest appendDimensionRequest = new AppendDimensionRequest();
        appendDimensionRequest.setSheetId(sheetID);
        appendDimensionRequest.setDimension("ROWS");
        appendDimensionRequest.setLength(length);

        Request request = new Request();
        request.setAppendDimension(appendDimensionRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(request);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public char getColumnNumberForText(String sheetName, String startColumn, String endColumn, String text) {

        String range = sheetName + "!" + startColumn + ":" + endColumn;
        ValueRange valueRange = new ValueRange();
        valueRange.setRange(range);
        ValueRange response = null;
        try {
            response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int counter = 0;
        boolean found_flag = false;
        assert response != null;
        List<Object> values = response.getValues().get(0);
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).toString().toUpperCase().equals(text.toUpperCase())) {
                counter = i;
                found_flag = true;
                break;
            }
            found_flag = false;
        }
        char valueStartColumn = '0';
        if (found_flag) {
            valueStartColumn = (char) ((int) 'A' + counter);
        }
        return valueStartColumn;
    }

    public int getRowNumberForText(String sheetName, String startRow, String endRow, String text) {
        String range = sheetName + "!" + startRow + ":" + endRow;
        ValueRange valueRange = new ValueRange();
        valueRange.setRange(range);
        valueRange.setMajorDimension("COLUMNS");
        ValueRange response = null;
        try {
            response = service.spreadsheets().values().get(spreadsheetId, range).setMajorDimension("COLUMNS").execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int counter = 0;
        List<Object> values = response.getValues().get(0);
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).toString().toUpperCase().equals(text.toUpperCase())) {
                counter = i;
                break;
            }
        }
        return counter;
    }

    public void insertColumn(String sheetName, int startIndex, int endIndex, boolean inheritFromBefore) {

        DimensionRange dimensionRange = new DimensionRange();
        dimensionRange.setSheetId(getSheetIDfromSheetName(sheetName));
        dimensionRange.setDimension("COLUMNS");
        dimensionRange.setStartIndex(startIndex);
        dimensionRange.setEndIndex(endIndex);

        InsertDimensionRequest insertDimensionRequest = new InsertDimensionRequest();
        insertDimensionRequest.setRange(dimensionRange);
        insertDimensionRequest.setInheritFromBefore(inheritFromBefore);

        Request request = new Request();
        request.setInsertDimension(insertDimensionRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(request);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setNoteandFormat(String a1Notation, String note) {

        HashMap<String, Integer> index = a1NotationSheetandGridProperties(a1Notation);
        int startRow = index.get("row");
        int endRow = startRow+1;
        int startColoumn = index.get("column");
        int endColoumn = startColoumn+1;

        int sheetID = index.get("sheetID");

        GridRange gr = new GridRange();
        gr.setStartRowIndex(startRow);
        gr.setEndRowIndex(endRow);
        gr.setStartColumnIndex(startColoumn);
        gr.setEndColumnIndex(endColoumn);
        gr.setSheetId(sheetID);

        CellFormat cellFormat = new CellFormat();
        cellFormat.setVerticalAlignment("middle");
        cellFormat.setHorizontalAlignment("center");

        CellData cellData = new CellData();
        cellData.setUserEnteredFormat(cellFormat);
        cellData.setNote(note);

        RepeatCellRequest repeatCellRequest = new RepeatCellRequest();
        repeatCellRequest.setRange(gr);
        repeatCellRequest.setCell(cellData);
        //field name to be given only if you want to update only a single field, if given as '*' it will update all the properties like value and set only the note.
        repeatCellRequest.setFields("note");

        Request req = new Request();
        req.setRepeatCell(repeatCellRequest);

        List<Request> requests = new ArrayList<>();
        requests.add(req);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(requests);

        try {
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getSheetIDfromSheetName(String sheetName){
        List<Sheet> spreadsheet = null;
        Integer sheetID = null;
        try {
            spreadsheet = service.spreadsheets().get(spreadsheetId).execute().getSheets();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(Sheet sheet : spreadsheet){
            if (sheet.getProperties().getTitle().toUpperCase().equals(sheetName.toUpperCase())){
                sheetID = sheet.getProperties().getSheetId();
//                System.out.println("Sheetname -> " + sheetName);
//                System.out.println("SheetID -> " + sheetID);
                break;
            }
        }
        return sheetID;
    }

    public HashMap<String, Integer> a1NotationSheetandGridProperties(String a1notation){

        List<String> ranges = new ArrayList<>();
        ranges.add(a1notation);

        Sheets.Spreadsheets.Get request = null;
        try {
             request = service.spreadsheets().get(spreadsheetId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        request.setRanges(ranges);
        request.setIncludeGridData(true);
        Spreadsheet response = null;
        try {
            response = request.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int columnNumber;
        int rowNumber;
        int sheetID;

        try {
            columnNumber = response.getSheets().get(0).getData().get(0).getStartColumn();
        } catch (NullPointerException e) {
            columnNumber = 0;
        }

        try {
            rowNumber = response.getSheets().get(0).getData().get(0).getStartRow();
        } catch (NullPointerException e) {
            rowNumber = 0;
        }

        sheetID = response.getSheets().get(0).getProperties().getSheetId();

        HashMap<String,Integer> index = new HashMap();
        index.put("row",rowNumber);
        index.put("column",columnNumber);
        index.put("sheetID",sheetID);

        return index;
    }

    public void clearRange(String range){
        ClearValuesRequest requestBody = new ClearValuesRequest();
        Sheets sheetsService = service;
        Sheets.Spreadsheets.Values.Clear request = null;
        ClearValuesResponse response = null;
        try {
            request = sheetsService.spreadsheets().values().clear(spreadsheetId, range, requestBody);
            response = request.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
    }


    //TODO : Setup enum for different formats

//    public static void main(String[] args) {
//        GoogleSheetsHelper googleSheetsHelper = new GoogleSheetsHelper("1yGOk_N0sHaiG8J_zf2GELafbHYWXmSQOKIWWnM9bHO0");
//        googleSheetsHelper.clearRange("ArtifactsMappings!F8");
//    }
}
