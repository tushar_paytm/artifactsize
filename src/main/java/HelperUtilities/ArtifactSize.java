package HelperUtilities;

import net.iharder.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ArtifactSize {

    public static String baseURL = "https://android-ci01.mypaytm.com:8040/artifactory/api/storage";
    public static String fileExt = ".aar";
    public static DefaultHttpClient httpclient = new DefaultHttpClient();
    public static Map<String, String> artifactdetails = new HashMap<String, String>();
    //public static Map<String,String> executionList =  new HashMap<String, String>();


    public static long getArtifactSizeRequest(String artifactName, String artifactPath, String artifactVersion) {

        String uri = baseURL + "/" + artifactName + "/" + artifactPath + "/" + artifactVersion + "/" + artifactName + "-" + artifactVersion + "" + fileExt;
        System.out.println("Request uri : " + uri);
        HttpGet getRequest = new HttpGet(uri);
        return fetchArtifactSize(getRequest);
    }

    public static long getCustomArtifactURLRequest(String uri){
        System.out.println("Request uri : " + uri);
        HttpGet getRequest = new HttpGet(uri);
        return fetchArtifactSize(getRequest);
    }

    public static long fetchArtifactSize(HttpGet httpGet) {
        httpGet.setHeader("Authorization", "Basic " + ArtifactSize.getEncodingString());
        HttpResponse response = null;
        String responseString = null;
        try {
            response = ArtifactSize.httpclient.execute(httpGet);
            responseString = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONParser parser = new JSONParser();
        JSONObject resp = null;
        try {
            resp = (JSONObject) parser.parse(responseString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long size = Long.parseLong(String.valueOf(resp.get("size")));
        return size;
    }


    public static String getEncodingString() {
        String encoding = Base64.encodeBytes(("dileep.bhadauria:paytm@123").getBytes());
        //String encoding = Base64.encodeBytes ((userInput.userName():userInput.password()).getBytes());

        return encoding;
    }


}
