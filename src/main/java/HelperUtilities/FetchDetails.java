package HelperUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class FetchDetails {

    static private List<Object> bName = new ArrayList<>();
    static private List<List<Object>> buildNameforSheet = new ArrayList<>();

    public GoogleSheetsHelper fetchandPushData(String sheetID, String mapping_sheetName, String final_sheetName, String fullName,
                                                                      String sumCellName, String artifactStartCell, boolean pushDatainMetabase, String mapper_column) {

        String propFileName = "customURL.properties";
        ArtifactVersionsParser artifactVersionsParser = new ArtifactVersionsParser();
        HashMap<Object, String> customeURIMap = artifactVersionsParser.getPropValues(propFileName);

        Helper helper = new Helper();


        GoogleSheetsHelper googleSheetsHelper = new GoogleSheetsHelper(sheetID);

        //Check if entry for provided release Name is already in the Main Sheet
        char columnNo = googleSheetsHelper.getColumnNumberForText(final_sheetName, "A2", "2", fullName);
        if (columnNo != '0') {
            System.out.print("\"" + fullName + "\" already exists, enter NEW build name in format <appVersion>_<MB/RC/RC1/RC2>, eg. 8.10.0_MB or Type exit to exit the execution: ");
            Scanner scanner = new Scanner(System.in);
            fullName = scanner.nextLine();
            if (fullName.equalsIgnoreCase("exit")) {
                System.out.println("You opted to exit...");
                System.exit(0);
            } else if (fullName.isEmpty()) {
                System.out.println("Build name found empty...");
                System.exit(0);
            }
        }
        bName.add(fullName);
        buildNameforSheet.add(bName);

        //Adding column
        googleSheetsHelper.insertColumn(final_sheetName, 4, 5, false);
        googleSheetsHelper.sheetBatchWrite(final_sheetName, "E2", "E2", buildNameforSheet, GoogleSheetsHelper.ValueInputOption.USER_ENTERED);

        List<List<Object>> values = googleSheetsHelper.sheetRead(sheetID, mapping_sheetName + "!A2:E");

        HashMap<String, Pair<Float, String>> mapper = new HashMap<>();
        List<List<Object>> size_List_MB = new ArrayList<>();
        List<Object> size_MB = new ArrayList<>();
        List<List<Object>> size_List_Bytes = new ArrayList<>();
        List<Object> size_Bytes = new ArrayList<>();

        System.out.println("Total Row count --> " + values.size());
        char mapper_columnNo = googleSheetsHelper.getColumnNumberForText(mapping_sheetName, "A1", "1", mapper_column);
        System.out.println(mapper_column + " found in column : " + mapper_columnNo);
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                if (row.size() == 5) {
                    System.out.printf("%s, %s, %s, %s, %s\n", row.get(0), row.get(1), row.get(2), row.get(3), row.get(4));
                    long sizeInBytes;
                    try {
                        if (customeURIMap.containsKey(row.get(1))) {
                            String uri = customeURIMap.get(row.get(1)).replaceAll("<version>", row.get(4).toString());
                            sizeInBytes = ArtifactSize.getCustomArtifactURLRequest(uri);
                        } else {
                            sizeInBytes = ArtifactSize.getArtifactSizeRequest(row.get(1).toString(), row.get(3).toString(), row.get(4).toString());
                        }
                        String humanReadable = helper.humanReadableByteCountBin(sizeInBytes);
                        float mb = helper.bytesToMB(sizeInBytes);
                        System.out.println("Bytes : " + sizeInBytes);
                        System.out.println("Human Readable : " + humanReadable);
                        size_MB.add(humanReadable);
                        size_List_MB.add(size_MB);
                        size_Bytes.add(sizeInBytes);
                        size_List_Bytes.add(size_Bytes);
                        mapper.put(row.get(mapper_columnNo - 65).toString(), new Pair<>(mb, humanReadable));
                        System.out.println("--------------------------------------------------------------------------------------------------");
                    } catch (java.lang.IllegalArgumentException e) {
                        //e.printStackTrace();
                        System.out.println(" NOT ABLE TO DETERMINE ");
                        size_MB.add("NA");
                        size_List_MB.add(size_MB);
                        size_Bytes.add("NA");
                        size_List_Bytes.add(size_Bytes);
                        mapper.put(row.get(mapper_columnNo - 65).toString(), new Pair<>(0.0f, "NA"));
                        System.out.println("--------------------------------------------------------------------------------------------------");
                    }
                } else {
                    size_Bytes.add("NA");
                    size_List_Bytes.add(size_Bytes);
                }
                size_MB = new ArrayList<>();
                size_Bytes = new ArrayList<>();
            }
        }
//        googleSheetsHelper.sheetBatchWrite(mapping_sheetName, "F2", "", size_List_Bytes, GoogleSheetsHelper.ValueInputOption.USER_ENTERED);

        System.out.println("Writing value in the final sheet...");
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                Pair<Float, String> pair = mapper.get(row.get(mapper_columnNo - 65));
                if (pair != null) {
                    Float inMB = pair.getFirst();
                    String inHumanReadable = pair.getSecond();

                    //Writing in the final sheet
                    columnNo = googleSheetsHelper.getColumnNumberForText(final_sheetName, "A2", "2", fullName);
                    int rowNumFinal = googleSheetsHelper.getRowNumberForText(final_sheetName, "D1", "D", row.get(mapper_columnNo - 65).toString()) + 1;
                    googleSheetsHelper.setNoteandFormat(googleSheetsHelper.sheetBatchWrite(final_sheetName, String.valueOf(columnNo) + rowNumFinal, "",
                            helper.stringtoLLObjectConverter(String.valueOf(inMB)),
                            GoogleSheetsHelper.ValueInputOption.USER_ENTERED).getResponses().get(0).getUpdatedRange(), inHumanReadable);
                }
            }
        }
        System.out.println("Completed!!!");
        //Summing up size
        int sumStart = googleSheetsHelper.getRowNumberForText(final_sheetName, "B2", "B", artifactStartCell) + 3;
        int sumEnd = googleSheetsHelper.getRowNumberForText(final_sheetName, "B2", "B", sumCellName) + 1;
        List<Object> val = new ArrayList<>();
        List<List<Object>> sumValue = new ArrayList<>();
        val.add("=SUM(E" + sumStart + ":E" + sumEnd + ")");
        sumValue.add(val);
        googleSheetsHelper.sheetBatchWrite(final_sheetName, "E" + (sumEnd + 1), "", sumValue, GoogleSheetsHelper.ValueInputOption.USER_ENTERED);


        System.out.println("Pushing data to Metabase...");
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            for (List row : values) {
                Pair<Float, String> pair = mapper.get(row.get(mapper_columnNo - 65));
                if (pair != null) {
                    Float inMB = pair.getFirst();
                    if (pushDatainMetabase) {
                        pushDashboard.RegistrationSuccessful(fullName, row.get(0), row.get(1), row.get(4), inMB);
                        System.out.println("Data pushed!!!");
                    }
                    else {
                        System.out.println("Data NOT pushed!!!");
                    }
                }
            }
        }

        return googleSheetsHelper;
    }

}
