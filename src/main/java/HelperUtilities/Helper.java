package HelperUtilities;

import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

public class Helper {

    public List<List<Object>> stringtoLLObjectConverter(String value) {
        List<Object> first = new ArrayList<>();
        List<List<Object>> second = new ArrayList<>();
        first.add(value);
        second.add(first);
        return second;
    }


    //bytes to MB
    public float bytesToMB(long bytes) {
        float MEGABYTE = 1024 * 1024;
        float val = bytes / MEGABYTE;
        DecimalFormat df = new DecimalFormat("#.###");
        val = Float.parseFloat(df.format(val));
        System.out.println(val + " Mb");
        return val;
    }

    //Binary (1 K = 1,024)
    public String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.3f %ciB", value / 1024.0, ci.current());
    }

    //SI (1 k = 1,000)
    public String humanReadableByteCountSI(long bytes) {
        if (-1000 < bytes && bytes < 1000) {
            return bytes + " B";
        }
        CharacterIterator ci = new StringCharacterIterator("kMGTPE");
        while (bytes <= -999_950 || bytes >= 999_950) {
            bytes /= 1000;
            ci.next();
        }
        return String.format("%.2f %cB", bytes / 1000.0, ci.current());
    }

}
