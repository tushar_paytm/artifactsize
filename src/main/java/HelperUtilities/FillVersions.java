package HelperUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class FillVersions {

    static String propFileName = "artifactMapper.properties";
    static private String projectModuleName_a1NotationRange = "!C:C";
    static private List<Object> vName = new ArrayList<>();
    static private List<List<Object>> vNameforSheet = new ArrayList<>();
    boolean flagCall = false;

    public void fillVersionDetails(String sheetID, String mapping_sheetName) {
        HashMap<Object, String> versionMap;
        ArtifactVersionsParser artifactVersionsParser = new ArtifactVersionsParser();
        versionMap = artifactVersionsParser.getPropValues(propFileName);
        GoogleSheetsHelper googleSheetsHelper = new GoogleSheetsHelper(sheetID);
        List<List<Object>> values = googleSheetsHelper.sheetRead(sheetID, mapping_sheetName + projectModuleName_a1NotationRange);
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            char versionCol = getFilled(googleSheetsHelper, mapping_sheetName, sheetID);
            if (flagCall) {
                putValues(values, versionMap, googleSheetsHelper, versionCol, mapping_sheetName);
            } else {
                /*System.out.print("Versions already filled do you want to continue with already filled versions? [Y/N], default Yes : ");
                Scanner scanner = new Scanner(System.in);
                String overwrite = scanner.nextLine();
                if (overwrite.equalsIgnoreCase("N")) {*/
                    googleSheetsHelper.clearRange(mapping_sheetName + "!" + versionCol + "2:" + versionCol);
                    putValues(values, versionMap, googleSheetsHelper, versionCol, mapping_sheetName);
                /*}*/
            }
        }
    }

    public char getFilled(GoogleSheetsHelper googleSheetsHelper, String mapping_sheetName, String sheetID){
        char versionCol = googleSheetsHelper.getColumnNumberForText(mapping_sheetName, "A1", "1", "Version");
        List<List<Object>> versionVal =  googleSheetsHelper.sheetRead(sheetID, mapping_sheetName + "!" + versionCol + "2:" + versionCol + "");
        if (versionVal == null) {
            flagCall = true;
        }
        return versionCol;
    }

    public void putValues(List<List<Object>> values, HashMap versionMap, GoogleSheetsHelper googleSheetsHelper, char versionCol, String mapping_sheetName) {
        for (int i = 0; i < values.size(); i++) {
            Object value = versionMap.get(values.get(i).get(0));
            vName.add(value);
            vNameforSheet.add(vName);
            if (value != null) {
                googleSheetsHelper.sheetBatchWrite(mapping_sheetName, String.valueOf(versionCol) + (i + 1), "",
                        vNameforSheet, GoogleSheetsHelper.ValueInputOption.RAW);
            }
            vName = new ArrayList<>();
            vNameforSheet = new ArrayList<>();
        }
    }
}
