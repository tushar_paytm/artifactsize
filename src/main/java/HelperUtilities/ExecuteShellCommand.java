package HelperUtilities;

import Core.Runner;

import java.io.*;
import java.util.HashMap;

public class ExecuteShellCommand extends Runner {
    public HashMap<String, Float> executeScript(String projectDir, String releaseName) {
        HashMap<String, Float> scriptValues = new HashMap<>();
        String modules[] = {"clean", "home", "appmanager", "paytmGamepind", "fetchBuildGradle"};
        Process p;
        for (int i = 0; i < modules.length; i++) {
            try {
                String[] cmd = {"sh", "src/main/script/fetchAarSize_Simplified.sh", projectDir, releaseName, modules[i]};
                p = Runtime.getRuntime().exec(cmd);
                p.waitFor();
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                    if (line.contains(" aarSize")) {
                        scriptValues.put(modules[i], convertValueinMB(line.split(":")[1].toString().trim()));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return scriptValues;
    }

    public Float convertValueinMB(String value) {
        if (value.contains("M")) {
            return Float.parseFloat(value.split("M")[0].trim());
        } else if (value.contains("K")) {
            return Float.parseFloat(value.split("K")[0].trim()) / 1000;
        }
        return 0F;
    }

}
