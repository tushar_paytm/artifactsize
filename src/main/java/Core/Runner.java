package Core;

import HelperUtilities.*;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.swing.*;
import java.util.HashMap;
import java.util.Scanner;

public class Runner {

    public static void main(String[] args) {

        String propFileName = "input.properties";
        ArtifactVersionsParser artifactVersionsParser = new ArtifactVersionsParser();
        HashMap<Object, String> versionMap = artifactVersionsParser.getPropValues(propFileName);
        String sheetID = versionMap.get("sheetID");
        String mapping_sheetName = versionMap.get("mapping_sheetName");
        String final_sheetName = versionMap.get("final_sheetName");
        String releaseName = versionMap.get("releaseName");
        String buildName = versionMap.get("buildName");
        String sumCellName = versionMap.get("sumCellName");
        String artifactStartCell = versionMap.get("artifactStartCell");
        String mapper_column = versionMap.get("mapper_column");
        String projectDirectory = versionMap.get("projectDirectory");
        boolean pushDataMetabase = false;

        Scanner scanInput = new Scanner(System.in);
        System.out.println("Do you want to push data to metabase:");
        System.out.print("Press enter if you DON'T want to push data else type Y and enter : ");
        String metaInput = scanInput.nextLine();
        if (metaInput.equalsIgnoreCase("Y")) {
            pushDataMetabase = true;
        }

        String fullName = releaseName + "_" + buildName;

        //User Driven build name
        System.out.print("Build name found as \"" + fullName + "\"");
        System.out.println("\n");
        System.out.print("Do you want to continue with same name? [Y/N] : ");
        String input = scanInput.nextLine();
        System.out.println("\n");
        if (input.equalsIgnoreCase("N")) {
            System.out.print("Enter new build version, eg. 8.10.0 : ");
            releaseName = scanInput.nextLine();
            while (releaseName.isEmpty()) {
                System.out.print("Build version cannot be empty, enter Build Version, eg. 8.10.0 : ");
                releaseName = scanInput.nextLine();
            }
            System.out.print("Enter new build phase, eg. MB/RC1/RC2 : ");
            buildName = scanInput.nextLine();
            while (buildName.isEmpty()) {
                System.out.print("Build phase cannot be empty, enter Build Phase, eg. MB/RC1/RC2 : ");
                buildName = scanInput.nextLine();
            }
            fullName = releaseName + "_" + buildName;
        } else if (!input.equalsIgnoreCase("Y")) {
            while (fullName.contains("null") || fullName.isEmpty()) {
                System.out.print("Build name cannot be empty or incomplete as \"" + fullName + "\", enter NEW build name in format <appVersion>_<MB/RC/RC1/RC2>, eg. 8.10.0_MB : ");
                fullName = scanInput.nextLine();
            }
        }
        System.out.println("Build Name as : " + fullName);

        ExecuteShellCommand executeShellCommand = new ExecuteShellCommand();
        HashMap<String, Float> scriptValues = executeShellCommand.executeScript(projectDirectory, "release-" + releaseName);
        System.out.println(scriptValues);

        FillVersions fillVersions = new FillVersions();
        fillVersions.fillVersionDetails(sheetID, mapping_sheetName);
        FetchDetails fetchDetails = new FetchDetails();
        GoogleSheetsHelper test = fetchDetails.fetchandPushData(sheetID, mapping_sheetName, final_sheetName, fullName, sumCellName, artifactStartCell, pushDataMetabase, mapper_column);

        Helper helper = new Helper();
        int homeRow = test.getRowNumberForText(final_sheetName, "D1", "D", "Home");
        int gamePindRow = test.getRowNumberForText(final_sheetName, "D1", "D", "Gamepind");
        int appManagerRow = test.getRowNumberForText(final_sheetName, "D1", "D", "App Manager");
        char column = test.getColumnNumberForText(final_sheetName, "A2", "2", fullName);
        test.sheetBatchWrite(final_sheetName, String.valueOf(column) + (homeRow+1), "", helper.stringtoLLObjectConverter(String.valueOf(scriptValues.get("home"))),
                GoogleSheetsHelper.ValueInputOption.USER_ENTERED);
        test.sheetBatchWrite(final_sheetName, String.valueOf(column) + (gamePindRow+1), "", helper.stringtoLLObjectConverter(String.valueOf(scriptValues.get("paytmGamepind"))),
                GoogleSheetsHelper.ValueInputOption.USER_ENTERED);
        test.sheetBatchWrite(final_sheetName, String.valueOf(column) + (appManagerRow+1), "", helper.stringtoLLObjectConverter(String.valueOf(scriptValues.get("appmanager"))),
                GoogleSheetsHelper.ValueInputOption.USER_ENTERED);

    }
}
